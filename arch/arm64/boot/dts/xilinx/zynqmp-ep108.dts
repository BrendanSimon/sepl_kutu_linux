/*
 * dts file for Xilinx ZynqMP ep108 development board
 *
 * (C) Copyright 2014 - 2015, Xilinx, Inc.
 *
 * Michal Simek <michal.simek@xilinx.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 */

/dts-v1/;

/include/ "zynqmp.dtsi"

/ {
	model = "ZynqMP EP108";

	aliases {
		serial0 = &uart0;
	};

	chosen {
		stdout-path = "serial0:115200n8";
	};

	memory {
		device_type = "memory";
		reg = <0x0 0x0 0x40000000>;
	};
};

&amba {
	misc_clk: misc_clk {
		compatible = "fixed-clock";
		#clock-cells = <0>;
		clock-frequency = <25000000>;
	};

	i2c_clk: i2c_clk {
		compatible = "fixed-clock";
		#clock-cells = <0x0>;
		clock-frequency = <111111111>;
	};

	sata_clk: sata_clk {
		compatible = "fixed-clock";
		#clock-cells = <0>;
		clock-frequency = <75000000>;
	};
};

&can0 {
	status = "okay";
	clocks = <&misc_clk &misc_clk>;
};

&gem0 {
	status = "okay";
	clocks = <&misc_clk>, <&misc_clk>, <&misc_clk>;
	phy-handle = <&phy0>;
	phy-mode = "rgmii-id";
	phy0: phy@0{
		reg = <0>;
		max-speed = <100>;
	};
};

&gpio {
	status = "okay";
	clocks = <&misc_clk>;
};

&i2c0 {
	status = "okay";
	clocks = <&i2c_clk>;
	clock-frequency = <400000>;
	eeprom@54 {
		compatible = "at,24c64";
		reg = <0x54>;
	};
};

&i2c1 {
	status = "okay";
	clocks = <&i2c_clk>;
	clock-frequency = <400000>;
	eeprom@55 {
		compatible = "at,24c64";
		reg = <0x55>;
	};
};

&sata {
	status = "okay";
	clocks = <&sata_clk>;
	ceva,broken-gen2;
};

&sdhci0 {
	status = "okay";
	clocks = <&misc_clk>, <&misc_clk>;
};

&sdhci1 {
	status = "okay";
	clocks = <&misc_clk>, <&misc_clk>;
};

&spi0 {
	status = "okay";
	clocks = <&misc_clk &misc_clk>;
	num-cs = <1>;
	spi0_flash0: spi0_flash0@0 {
		compatible = "m25p80";
		#address-cells = <1>;
		#size-cells = <1>;
		spi-max-frequency = <50000000>;
		reg = <0>;

		spi0_flash0@00000000 {
			label = "spi0_flash0";
			reg = <0x0 0x100000>;
		};
	};
};

&spi1 {
	status = "okay";
	clocks = <&misc_clk &misc_clk>;
	num-cs = <1>;
	spi1_flash0: spi1_flash0@0 {
		compatible = "m25p80";
		#address-cells = <1>;
		#size-cells = <1>;
		spi-max-frequency = <50000000>;
		reg = <0>;

		spi1_flash0@00000000 {
			label = "spi1_flash0";
			reg = <0x0 0x100000>;
		};
	};
};

&uart0 {
	status = "okay";
	clocks = <&misc_clk &misc_clk>;
};

&usb0 {
	status = "okay";
	dr_mode = "peripheral";
	maximum-speed = "high-speed";
};

&usb1 {
	status = "okay";
	clocks = <&misc_clk>, <&misc_clk>;
	dr_mode = "host";
	maximum-speed = "high-speed";
};

&watchdog0 {
	status = "okay";
	clocks= <&misc_clk>;
};
